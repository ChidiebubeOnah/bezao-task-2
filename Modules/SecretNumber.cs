﻿using System;
using BEZAO___Task_2.Interfaces;

namespace BEZAO___Task_2.Modules
{
    public class SecretNumber
    {
        private readonly ILogger _logger;

        public SecretNumber(ILogger logger)
        {
            _logger = logger;
        }

        public void Run()
        {
            for (int i = 4; i >= 1; i--)
            {
                var rand = new Random();
                var secret_num = rand.Next(1, 10);

                _logger.Log("Guess my Secret Number: ");
                var Guess = Console.ReadLine();

                if (int.Parse(Guess) == secret_num)
                {
                    _logger.Log("You Won");
                    break;
                }
                else
                {
                    _logger.Log("You Lost " + (i - 1) + " Chance(s) Left");


                }

            }
        }

    }
}
