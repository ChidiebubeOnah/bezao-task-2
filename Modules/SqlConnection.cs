using BEZAO___Task_2.Interfaces;
using BEZAO___Task_2.Services;

namespace BEZAO___Task_2.Modules
{
    public class SqlConnection : DbConnectionService
    {
        private readonly ILogger _logger;

        public SqlConnection(string dbConnection) : base(dbConnection)
        {
            _logger = new ConsoleLoggerService();
        }
        public override void Open()
        {

            _logger.Log("---------------------------------------------");

            _logger.Log("SqlConnection Open and Running.....");
        }

        public override void Close()
        {
            _logger.Log("---------------------------------------------");
            _logger.Log("SqlConnection service Stopped\nConnection Closed!.....");
            _logger.Log("---------------------------------------------");
        }
    }
}
