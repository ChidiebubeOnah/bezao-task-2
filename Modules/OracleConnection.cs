using BEZAO___Task_2.Interfaces;
using BEZAO___Task_2.Services;

namespace BEZAO___Task_2.Modules
{
    public class OracleConnection : DbConnectionService
    {
        private readonly ILogger _logger;

        public OracleConnection(string dbConnection) : base(dbConnection)
        {
            _logger = new ConsoleLoggerService();
        }
        public override void Open()
        {
            _logger.Log("OracleConnection Open and Running.....");
            _logger.Log("---------------------------------------------");
        }

        public override void Close()
        {
            _logger.Log("---------------------------------------------");
            _logger.Log("OracleConnection service Stopped\nConnection Closed!.....");
            _logger.Log("---------------------------------------------");
        }
    }
}
