﻿using System;
using BEZAO___Task_2.Interfaces;

namespace BEZAO___Task_2.Modules
{
   public class MirrorText
    {
        private readonly ILogger _logger;
        private  string Name { get; set; }
        private  int Length_ { get; set; }
        private string ReversedWord { get; set; }


        public MirrorText(ILogger logger)
        {
            _logger = logger;
        }

        public void Run()
        {
            _logger.Log("Enter Your Name and Will reverse it: ");
            Name = Console.ReadLine();
            if (Name != null)
            {
                Length_ = Name.Length;
                var charList = new char[Length_];
                for (int i = 0; i < Length_; i++)
                {
                    charList[i] = Name[i];
                }
                Array.Reverse(charList);

                foreach (var charc in charList)
                {
                    ReversedWord += charc;
                }
                _logger.Log(ReversedWord);
            }

            
        }
    }
}
