﻿using System;
using System.Collections.Generic;
using BEZAO___Task_2.Interfaces;

namespace BEZAO___Task_2.Modules
{
    public class UniqueNumbers
    {
        private readonly ILogger _logger;
        private readonly List<int> _numbers;
        private string Input { get; set; }
       

        public UniqueNumbers(ILogger  logger)
        {
            _logger = logger;
            _numbers = new List<int>();
        }

        public void Run()
        {


            _logger.Log("Enter 5 numbers: ");
            while (_numbers.Count < 5)
            {
                Input = Console.ReadLine();
                if (_numbers.Contains(int.Parse(Input ?? throw new InvalidOperationException())))
                {
                    _logger.Log(int.Parse(Input) + " Already exists, Try again");
                    continue;
                }
                _numbers.Add(int.Parse(Input));

            }
            _numbers.Sort();
            _logger.Log("Output\n-------------------------------------");
            foreach (var item in _numbers)
            {
                _logger.Log(item.ToString());
            }
        }
    }
}