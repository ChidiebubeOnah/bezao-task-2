﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAO___Task_2.Menus;
using BEZAO___Task_2.Services;

namespace BEZAO___Task_2
{
    class Program

    {
        static void Main(string[] args)
        {
            var app = new Menu(new ConsoleLoggerService());
            app.Run();
        }
    }
}
