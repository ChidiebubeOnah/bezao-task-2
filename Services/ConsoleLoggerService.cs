﻿using System;
using BEZAO___Task_2.Interfaces;

namespace BEZAO___Task_2.Services
{
    public class ConsoleLoggerService: ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}