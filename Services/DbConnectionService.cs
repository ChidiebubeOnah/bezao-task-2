using System;

namespace BEZAO___Task_2.Services
{
    public abstract class DbConnectionService
    {
        
        public string ConnectionString { get; set; }
        


        public DbConnectionService(string dbConnection)
        {
            if (String.IsNullOrWhiteSpace(dbConnection))
                throw new InvalidOperationException("Can't Initialize with null value");
            ConnectionString = dbConnection;
        }

        public abstract void Open();

        public abstract void Close();


    }
}
