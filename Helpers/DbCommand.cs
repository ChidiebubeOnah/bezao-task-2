using System;
using BEZAO___Task_2.Interfaces;
using BEZAO___Task_2.Services;

namespace BEZAO___Task_2.Helpers
{
    public class DbCommand
    {
        private readonly DbConnectionService _dbConnection;
        private readonly ILogger _logger; 

        public DbCommand(DbConnectionService dbConnection, string instruction)
        {
            if (dbConnection == null)
                throw new InvalidOperationException("Null value supplied");
            _logger = new ConsoleLoggerService();
            _dbConnection = dbConnection;

            Execute(instruction);
        }

        private void Execute(string instruction)
        {

            if (String.IsNullOrWhiteSpace(instruction))
                throw new InvalidOperationException("command can't be null");

            _dbConnection.Open();
            _logger.Log("---------------------------------------------");
            _logger.Log($"Executing {instruction} command\n\t{instruction} Executed");
            _logger.Log("---------------------------------------------");
            _dbConnection.Close();

        }
    }
}
