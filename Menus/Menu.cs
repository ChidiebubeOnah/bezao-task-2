﻿using System;
using BEZAO___Task_2.Interfaces;
using BEZAO___Task_2.Modules;
using BEZAO___Task_2.Services;
using SqlConnection = System.Data.SqlClient.SqlConnection;

namespace BEZAO___Task_2.Menus
{
    public class Menu
    {
        private readonly ILogger _logger;
        private string _choice;

        private SecretNumber SecretNumber { get; set; }
        private MirrorText MirrorText { get; set; }

        private UniqueNumbers UniqueNumbers { get; set; }

        private DbMenu DbMenu { get; set; }
        public Menu(ILogger logger)
        {
            _logger = logger;
        }
        public void Run()
        {
            
            _logger.Log("\nWelcome to the BEZAO_Task_2 Interactive Menu.\n---------------------------------------------" +
                        "\nSelect an Option to get Started:\n1." +
                        " Guess the Secret number\n2. Mirror Text\n3. Get Unique Numbers\n4. Set DbConnection\n5." +
                        " Exit\n---------------------------------------------");
            _choice = Console.ReadLine();

            MenuOptions(_choice);


        }
        private void MenuOptions(string choice)
        {
            switch (choice)
            {
                case "1":
                    SecretNumber = new SecretNumber(new ConsoleLoggerService());
                    SecretNumber.Run();
                    break;
                case "2":
                    MirrorText = new MirrorText(new ConsoleLoggerService());
                    MirrorText.Run();
                    break;
                case "3":
                    UniqueNumbers = new UniqueNumbers(new ConsoleLoggerService());
                    UniqueNumbers.Run();
                    break;
                case "4":
                    DbMenu = new DbMenu(new ConsoleLoggerService());
                    DbMenu.Run();
                    break;
                case "5":
                    _logger.Log("Thanks for You Using the APP!!!");
                    break;
                default: 
                    _logger.Log("invalid Input....Try Again!!!");
                    break;
            }
        }

    }
}