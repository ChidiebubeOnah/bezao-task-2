﻿using System;
using BEZAO___Task_2.Helpers;
using BEZAO___Task_2.Interfaces;
using BEZAO___Task_2.Modules;

namespace BEZAO___Task_2.Menus
{
    public class DbMenu
    {
        private readonly ILogger _logger;
        private string _choice;
        private SqlConnection SqlConnection { get; set; }
        private OracleConnection OracleConnection { get; set; }
        private DbCommand SqlCommand { get; set; }
        private DbCommand OracleCommand { get; set; }


        public DbMenu(ILogger logger )
        {
            _logger = logger;
        }

        public void Run()
        {
            _logger.Log("\nWelcome to the BEZAO_Task_2 DB Menu.\n---------------------------------------------" +
                        "\nSelect an Option to get Started:\n1." +
                        " Create SQL Db\n2. Create Oracle." +
                        " \n3. Exit\n---------------------------------------------");
            _choice = Console.ReadLine();

            MenuOptions(_choice);


        }

        private void MenuOptions(string choice)
        {
            switch (choice)
            {
                case "1":
                    SqlConnection = new SqlConnection("127.0.1//@mysys.db");
                    SqlCommand = new DbCommand(SqlConnection, "Create");
                    break;
                case "2":
                    OracleConnection= new OracleConnection("127.0.1//@myOrL.db");
                    OracleCommand = new DbCommand(OracleConnection, "Create Db");
                    break;

                case "3":
                    _logger.Log("Thanks for You Using the APP!!!");
                    break;
                default:
                    _logger.Log("invalid Input....Try Again!!!");
                    break;
            }
        }
    }
}